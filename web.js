var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');

var domain = require('./domain.js');

var app = express();
var port = process.env.PORT || 8181;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));

routes = express.Router();
require("./routes")(routes, domain);

app.use('/', routes);
app.listen(port);
