module.exports = function(router, domain) {

  router.get('/', (req, res) => {
    res.send("Hello world!");
  });

  router.post('/jwt', (req, res) => {
    res.send(domain.generateTokenFromBody(req.body));
  });

};
