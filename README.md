# jwt-rsa-rs-generator
Generates RSA signed Json Web Token from given body.

# Requirements
* NodeJS 6+
* A RSA key generator tool (OpenSSH, ssh-agent, etc)

# Usage
* Generate an RSA private key (SHA 256, RSA 2048) via OpenSSH or ssh-agent (or any ssh tool). Export it to "./private.key".
* Generate RSA public key corresponding to your private key.
* Lunch the rest service.
```bash
> node web.js
```
* Generate the JWT
```bash
POST localhost:8181/jwt

body:

{
  "scope": [
    "mainrealm.write",
    "mainrealm.read",
    "ROLE_ADMIN"
  ],
  "user_id": "opet69",
  "user_name": "opet69",
  "email": "abc@example.com"
}
```

* Use the token in your system and valid the signature with the public key.
* Enjoy.
