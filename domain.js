var uuid = require('uuid');
var nJwt = require('njwt');
var fs = require('fs');

var domain = {};
domain.key = fs.readFileSync('./private.key');

domain.generateTokenFromBody = function(body) {
  var claims = body;
  var jwt = nJwt.create(claims, domain.key, "RS256");
  var token = jwt.compact();

  return token;
};

module.exports = domain;
